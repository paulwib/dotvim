" Enable syntax highlighting and settings for natively supported languages
if has("syntax")
  syntax on
  filetype plugin indent on
endif

" Markdown: highlighting in fenced code blocks
let g:markdown_fenced_languages = ['html', 'javascript', 'bash=sh']

