" nerdtree
"
" Ctrl-n to Display the file browser tree
nmap <C-n> :NERDTreeToggle<CR>

" ,p to show current file in the tree
nmap <leader>p :NERDTreeFind<CR>

